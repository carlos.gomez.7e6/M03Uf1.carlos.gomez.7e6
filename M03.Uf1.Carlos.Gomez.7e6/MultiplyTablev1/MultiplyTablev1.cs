﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Volem printar les taules de multiplicar.

L'usuari introdueix un enter

Mostra per pantalla la taula de multiplicar del número introduït:
*/
public class MultiplyTablev1
{
    private static void Main()
    {
        int y = Convert.ToInt32(Console.ReadLine());//declaro un int i el demano
        int x = 1;//declaro un int i l'inicialitzo
        do
        {
            Console.WriteLine(x + "*" + y + "=" + x * y);//print la taula de multiplicar
            x++;//sumo 1 a x

        } while (x <= 9);//mentres x sigui mes petit o igual que 9
    }
}