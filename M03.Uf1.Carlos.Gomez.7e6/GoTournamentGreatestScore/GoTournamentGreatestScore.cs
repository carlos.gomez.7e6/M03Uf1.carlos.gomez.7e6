﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introduirà el nom del participant i la puntuació, cada un en una línia.

Quan ja no hi hagi més participants entrara la paraula clau END.

Imprimeix per pantalla el guanyador del concurs i els punts obtinguts.
*/
public class GoTournamentGreatestScore
{
    private static void Main()
    {
        string Nom = ""; //declaro e inicialitzo totes les variables que necesito
        int punt = 0;
        int punt_max = 0;
        string nom_winner = Nom;

        while (Nom != "END")//si nom no es en entra al while
        {
            if (punt > punt_max)//si la puntuacio es mes gran que puntuacio max entra al if
            {
                
                punt_max = punt; //igualo punt max a la nova puntuacio
                nom_winner = Nom; //poso el nom del actual guanyador

            }
            Nom = Convert.ToString(Console.ReadLine());//demano un nom del concursant
            if(Nom !="END") punt = Convert.ToInt32(Console.ReadLine());//faig un if que si es END no demani puntuacio
        }

        if (Nom == "END" && punt == 0) Console.WriteLine("No se ha introducido ningun participante");// si es end amb puntuacio 0 es molt provable que no s'hagi ficat ningun participant
        else Console.WriteLine(nom_winner + ":" + punt_max);//printo el nom y la puntuacio
       
    }
}