﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Imprimeix les taules de multiplicar en forma de taula
*/
public class MultiplyTableFull
{
    private static void Main()
    {
        for (int i= 1; i < 10; i++)//faig un for on declaro e inicializo a 1 i, em compari que i es mes petit que 10 y sumi 1
        {
            Console.WriteLine("\t");//faig una linea
            ;
            for (int j = 1; j < 10; j++)//faig un for on declaro e inicializo a 1 j, em compari que j es mes petit que 10 y sumi 1
            {
                Console.Write(j * i + "\t");//multiplico els valors y els escric a la linea
            }
        }
    }
}