﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Demana un enter a l'usuari.

Imprimeix per pantalla tants punts com l'usuari hagi indicat
*/
public class DotLine
{
    private static void Main()
    {
        Console.WriteLine("Donam un numero enter");//printo una ordre a usuari
        int x=Convert.ToInt32(Console.ReadLine());//demano un numero enter al usuari
        {
            for(int y= 0; y < x; y++)//aquest for em fa de contador fins arribar a x y em suma +1 a y cada vegada
            {
                Console.Write(".");//printo un puntet
            }
        }
    }
}
