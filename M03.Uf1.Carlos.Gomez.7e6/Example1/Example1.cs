﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Printa en pantalla les sumes de 1 fins arribar a 20 i a 1000
*/
public class Example1
{
    private static void Main()
    {
        int x = 1;//declaro e incialitzo a 1 un int

        while (x <= 20)//mentres x sigui mes petit o igual que 20
        {
            Console.Write(x+","); //printo per pantalla
            x++;//sumo 1 a x
        }
        x=1;
        Console.WriteLine();
        while (x <= 1000)//mentres x sigui mes petit o igual que 1000
        {
            Console.Write(x + ",");//printo per pantalla
            x++;//sumo 1 a x
        }
    }
}
