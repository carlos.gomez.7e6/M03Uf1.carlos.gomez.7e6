﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5.

L'usuari introdueix enters.

Quan introdueixi el 5 imprimeix 5 trobat!.
*/
public class WaitFor5
{
    private static void Main()
    {
        int x;//declaro un int x
        do
        {
            Console.WriteLine("Escriume un 5");//printo un ordre
            x = Convert.ToInt32(Console.ReadLine());//declaro un int i el demano
        } while (x != 5);//mentres x no sigui 5
     
        Console.WriteLine("Merci");//printo un feedback final
    }
}
