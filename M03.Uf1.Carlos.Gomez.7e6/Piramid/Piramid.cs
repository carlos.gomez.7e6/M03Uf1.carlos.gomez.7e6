﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:S'imprimeix una piràmide d'altura N de #
*/
public class Piramid
{
    private static void Main()
    {
        Console.WriteLine("Donam un numero per l'altura de la piramide");//printo una ordre
        int x= Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo demanantli a l'usuari l'altura de la piramide

        for(int i=1; i <= x; i++)//declaro e inicializo un contador que em compara que i sigui mes petit o igual a x
        {
            Console.WriteLine();//mescriu una linea
            for (int j=0; j < i; j++)//j a de ser mes petit que i
            {
                Console.Write("#");//mescriu un #
            }
        }
    }
}