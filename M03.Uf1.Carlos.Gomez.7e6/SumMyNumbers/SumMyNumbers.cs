﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Fer un programa que llegeixi un nombre enter positiu n i escrigui la suma de les seves xifres. Per exemple les xifres del nombre 456 sumen (4+5+6) = 15
*/

public class SumMyNumbers
{
    private static void Main()
    {
        Console.WriteLine("Donam un nombre y tel sumo");//printo una ordre
        int x = Convert.ToInt32(Console.ReadLine());//declaro y demano un numero al usuari
        int resultado = 0;//declaro e inicialitzo un int resultado
        while(x!=0)//mentres x no sigui 0 em fara el bucle
        {
            int y = x % 10;//declaro e inicialitzo y i l'igualo al modul 10 de x per separar el primer nombre
            Console.WriteLine(y);//printo la variable
            resultado += y;//sumo y a resultado
            x = x / 10;//divideixo per 10 per conseguir el seguent nombre
        }
        Console.WriteLine(resultado); //printo el resultat
    }
}