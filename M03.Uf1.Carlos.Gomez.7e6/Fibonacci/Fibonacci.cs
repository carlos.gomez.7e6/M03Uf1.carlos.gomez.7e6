﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Donat un nombre enter positiu, escriu els nombres de Fibonacci (Lleonard de Pisa) inferiors o iguals a ell.
Els nombres de Fibonacci es defineixen de la manera següent: El primer és 0, el segon és 1, el següent és la suma dels dos anteriors i així successivament.
*/
public class Fibonacci
{
    private static void Main()
    {
        int i = Convert.ToInt32(Console.ReadLine());
        int a = 0;//declaro tres variables on a, b seran els dos primers nombres y c fará de recipient
        int b = 1;
        int c = 0;
        while(c <= i)
        {
            Console.WriteLine(c);//printo c
            c = b; //igualo el recipient amb el segon nombre
            b = a + b;// b sera igual al primer nombre + el segon
            a = c;// a l'igualo al recipien que conté b
        }
    }
}