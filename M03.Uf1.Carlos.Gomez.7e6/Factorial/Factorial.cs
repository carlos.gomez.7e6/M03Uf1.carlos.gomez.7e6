﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Calcula el factorial d'un valor
*/

public class Factorial
{
    private static void Main()
    {
        int n = Convert.ToInt32(Console.ReadLine());//declaro y demano a lusuari un valor
        int y = 1;//declaro e inicialtzo a 1
        for(int i = 1; i <= n; i++)//declaro e incialitzo a 1 i, comparo i sigui mes petit o igual que n, sumo 1 a i
        {
           y = y * i;//igualo y a y multiplat per i 
        }
        Console.WriteLine(y);//escric el resultat
    }
}