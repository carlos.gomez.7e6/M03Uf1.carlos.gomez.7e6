﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/25
* DESCRIPTION:Implementa un programa que mostre true si el nombre introduït és primer, false en qualsevol altre cas.

Clarificació: Els números 0 i 1 no són primers.

*/
public class IsPrime
{
    private static void Main()
    {
        Console.WriteLine("Donam un numero que no sigui ni 0 ni 1 y et dire si es primer");//printo una ordre
        int n = Convert.ToInt32(Console.ReadLine());//declaro n i la demano al usuari
        int contador=0;//declaro e incialitzo un int contador a 0
        
        while(n ==0 || n == 1)//mentres n sigui 0 o 1
        {
            Console.WriteLine("Donam un numero que no sigui 0 o 1");//printo una ordre
            n=Convert.ToInt32(Console.ReadLine());//demano n a l'usuario
        }

        for (int i = 2; i <= n; i++)//declaro i e la incialitzo a 2; mentres i sigui mes petita o igual a n; sumo 1 i
        {
            if (n % i == 0)//si el modul i a n es 0
            {
                contador++;// li sumo 1 al contador
            }
        }

        if(contador == 1)//si el contador es 1 (significa que l'unic valor que li ha 0 al modul es el seu mateix numero, es a dir, es primer)
        {
            Console.WriteLine(n + " es Primer");//printo el resultat mes no es primer
        }
        else//sino
        {
            Console.WriteLine(n + " no es Primer");//printo el resultat mes no es primer
        }

       
    }
}