﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introduiex un text per pantalla que pot tenir salts de línia.

Per indicar que ha acabat d'introduïr el text escriurà una línia amb la paraula clau END

Imprimeix el nombre de línies que ha introduït l'usuari (sense contar la END
*/
public class HowManyLines
{
    private static void Main()
    {
        string x = Convert.ToString(Console.ReadLine());//declaro e inicialitzo un string sera el que demani
        int y = 0;//declaro e inicialitzo un int que sera el contador

        while (x != "END")//comparo si x no es END
        {
            x= Convert.ToString(Console.ReadLine());//demano un valor pel string
            y++;//aumento el contador
        }
        Console.WriteLine(y);//printo el numero de linees
    }
}
//Realment l'end si el conto dintre el while, l'unic que no conto es quan ho dic al principi de tot 
// Y si no dic end no conto igualment perque si el dic dintre del while si el contare
// Doncs fent-ho aixi al final de tot sempre em sortiran el numero de linees sense l'END