﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/
public class LetsCount
{
    private static void Main()
    {
        int x = Convert.ToInt32(Console.ReadLine()); //demano un número
        int y = 1;

        while(y <= x)//utilitzo el while per comparar el número que demano amb el numero inicial
        {
            Console.Write(y +"\t");//escric el numero inicial y consecotius
            y++; //increment del numero inicial
        }

    }
}