﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1
*/
public class CountDown
{
    private static void Main()
    {
        Console.WriteLine("Donam un numero enter");//printo una ordre
        int x = Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo un variable int demanantli a l'usuari un valor
        
        for ( ; x > 0; x--)//faig un bucle comparant x es mes gran que 0 y restant 1 cada vegada
        {
            Console.Write(x + " ");//printo x
        }
        
    }
}