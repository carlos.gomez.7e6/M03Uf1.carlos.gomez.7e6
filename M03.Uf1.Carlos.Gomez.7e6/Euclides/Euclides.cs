﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.
*/
public class Euclides
{
    private static void Main()
    {
        Console.WriteLine("Donam un valor positiu");
        int x = Convert.ToInt32(Console.ReadLine());//declaro y demano un valor
        Console.WriteLine("Donam un altre valor positiu");
        int y = Convert.ToInt32(Console.ReadLine());//declaro y demano un valor 

        while (x%y != 0)//un bucle que diu que si el modul y de x no es 0 fasi
        {
            int r = x % y;//declaro e igual r a el modul x de y per quedarme amb el residu
            x = y;//igualo x a y per a que y es comberteixi en el divident
            y = r;//ara igualo y per a que el divisor sigui el residu
        }
        Console.WriteLine(y + " es este");// aquest seria el M.C.D
    }
}
