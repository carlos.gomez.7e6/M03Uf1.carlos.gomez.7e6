﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Volem printar les taules de multiplicar.

L'usuari introdueix un enter

Mostra per pantalla la taula de multiplicar del número introduït:
*/
public class MultiplyTable
{
    private static void Main()
    {
        int y = Convert.ToInt32(Console.ReadLine());//demano el numero de la taula que vol
        int x = 1;//inicialitzo el numero que será multiplicat
        while (x <= 9)//faig el while perque em fasi 9 multiplicacions
        {
            Console.WriteLine(x + "*" + y + "=" + x*y); //escric la operacio matematica amb el resultat
            x++;//augmento en 1 el numero multiplicat per fer la taula
        }
    }
}