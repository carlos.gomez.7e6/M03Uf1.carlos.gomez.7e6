﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Imprimeix tots els nombres enters divisibles per 3 que hi ha entre A i B (inclusiu).
*/
public class DivisibleBy
{
    private static void Main()
    {
        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());
        if(x > y)//faig un intercanvi de valor per si x es mes que y 
        {
            int c = x;
            x = y;
            y = c;

            while (x <= y)//Si x es mes petit que y fa bucle
            {
                if (x % 3 == 0)//Comprova que modul 3 de x es 0
                {
                    Console.WriteLine(x);//printa el numero x
                    x += 3;//li suma a x 3
                }
                else
                {
                    x += 1;// li suma 1 a x per poder comprobar si el seguent numero es multiple de 3
                }
            }
        }
        else
        {
            while (x <= y)//Si x es mes petit que y fa bucle
            {
                if (x % 3 == 0)//Comprova que modul 3 de x es 0
                {
                    Console.WriteLine(x);//printa el numero x
                    x += 3;//li suma a x 3
                }
                else
                {
                    x += 1;// li suma 1 a x per poder comprobar si el seguent numero es multiple de 3
                }
            }
        }
        
    }
}