﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/25
* DESCRIPTION:S'imprimeix una piràmide d'altura N de # centrada
*/
public class Piramid
{
    private static void Main()
    {
        Console.WriteLine("Dame la altura de la piramide: ");//printo una ordre
        int n = Convert.ToInt32(Console.ReadLine());//declaro i demano un int

        for (int i = 1; i <= n; i++)//declaro e inicialitzo a 1 i; mentres i sigui mes petita o igual a n; sumo 1 a i
        {
            int spaces = n - i;//declaro spaces i la incialitzo a n - i (es com si li restés el nombr de hashtags)

            for (int j = 1; j <= i; j++)//declaro j i la inicialitzo a 1, mentres j sigui mes petita o igual a j; sumo 1 j
            {
                while (spaces > 0)//mentres spaces sigui mes gran que 0
                {
                    spaces--;//li resto 1 a space
                    Console.Write(" ");//printo un espai
                }
                Console.Write("# ");//printo un hashtag amb un espai
            }

            Console.WriteLine();//faig una salt de linea
        }
    }
}


