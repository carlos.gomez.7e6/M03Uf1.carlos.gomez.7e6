﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/
public class LetsCount
{
    private static void Main()
    {
        int x = Convert.ToInt32(Console.ReadLine());//declaro un int i el demano
        int y = 1;//declaro un int i el inicialitzo a 1
        do
        {
            Console.Write(y + "\t");//printo y
            y++;//sumo 1 a y
        } while (y <= x);// mentres y sigui mes gran o igual que x
       

    }
}