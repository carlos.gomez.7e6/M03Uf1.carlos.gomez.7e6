﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Donada una llista de lletres, imprimeix únicament les vocals que hi hagi.

L'entrada consta de dues parts:

Primer s'indica la quantitat de lletres

A continuació venen les lletres separades per espais en blanc. Imprimeix totes les que són vocals.
*/
public class OnlyVowels
{
    private static void Main()
    {
        Console.WriteLine("Quantes lletres m'escriuras?");//printo una ordre a la consola
        int numLl= Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo un int demanantli al usuari el numero de lletres que escriure

        for(int i = 1; i <= numLl *2 -1  ; i++)//faig un for on i la igualo a 1 y la comparo amb el numero de lletres.
        {                                       // la multiplico per 2 y resto 1 perque hi hauran tambe el numero de espais pero sense l'ultim perque es la key enter
            char letra = Convert.ToChar(Console.Read());// demano les lletres amb espais y les llegeixo

            if(letra == 'a' || letra =='e' || letra =='i' || letra =='o' || letra == 'u')//comparo si letra es una vocal
            {
                Console.WriteLine(letra);//si es aixi printo letra que segur que sera una vocal
            }
            
        }
    }
}