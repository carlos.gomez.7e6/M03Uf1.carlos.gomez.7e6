﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Escriu un programa que llegeixi un nombre natural més petit que 256 i escrigui la seva representació en binari.
*/
public class ToBinnary
{
    private static void Main()
    {
        int y = 0; //declaro e inicialitzo a 0 int y
        Console.WriteLine("Donam un numero y tel faig binari");
       int n =Convert.ToInt32(Console.ReadLine()); //declaro n y demano a l'usuari un numer
        Console.WriteLine();
       while (n != 0)// mentres n no sigui 0 em fara el bucle
        {
            y = (n % 2);// y = al modul 2 de n per conseguir el un numero del numero binari
            n = n / 2;//divideixo n per 2 per seguir amb les models y el bucle fins qe em dongui 0
            
            Console.WriteLine(y);//printo el resultat

        }
       Console.WriteLine();
        Console.WriteLine("Comença a llegir per sota :)");
    }
}