﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introdueix dos enters, la base i l'exponent. Imprimeix el resultat de la potencia (sense usar la llibreria math).
*/
public class ManualPow
{
    private static void Main()
    {
        Console.WriteLine("Escriume la base");//printo una ordre
        int _base = Convert.ToInt32(Console.ReadLine());//declaro y demano la base al usuari
        Console.WriteLine("Escriume l'exponent");//printo una ordre
        int exponent = Convert.ToInt32(Console.ReadLine());//declaro y demano l'exponent al usuari
        int resultat = _base;//declaro e igual resultat a base

        for(int i= 1; i < exponent; i++)//faig un bucle que em compari i es mes petit que exponent y em sumi i 1
        {
            resultat *= _base; //multiplico el resultat per la base
        }
        Console.WriteLine(resultat);//printo el resulat
    }
}