﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introdueix dos valors enters.

Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major
*/
public class LetsCount
{
    private static void Main()
    {
        int x = Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo un int demanant al usuari un valor
        int y = Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo un int demanant al usuari un valor

        if (x > y)// comparo el nombre x si es mes gran que y 
        {
            int c = x;
            x = y;
            y = c; // si es aixi intercambio els valors

            for(x+= 1;x < y; x++) //faig un fo per poder anants sumant a la variable x 1 y compararla amb y
            {
                Console.Write(x);//printo x
            }

        }
        else
        {
            for (x += 1; x < y; x++) //faig un fo per poder anant sumant a la variable x 1 y compararla amb y
            {
                Console.Write(x);//printo x
            }
        }
    }
}