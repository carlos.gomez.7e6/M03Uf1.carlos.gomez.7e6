﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Demanar a l'usuari un enter entre 1 i 5.

Si introdueix un número més gran o més petit, torna-li a demanar.

Mostra per pantalla: El número introduït: 3, (en cas de que l'usuari hagi introduït un 3) substituint el 3 pel número.
*/
public class NumberBetweenOneAndFivev1
{
    private static void Main()
    {
        int x = 0;
        do
        {
            Console.WriteLine("Donam un numero");//printo una ordre
            x = Convert.ToInt32(Console.ReadLine());//declaro un int i el demano

        } while (x > 5 || x < 1);//mentres x sigui mes gran que 5 o x sigui mes petit que 1

        Console.WriteLine(x + " trobat");//printo x i trobat

    }
}
