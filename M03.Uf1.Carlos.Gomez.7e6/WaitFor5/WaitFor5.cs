﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5.

L'usuari introdueix enters.

Quan introdueixi el 5 imprimeix 5 trobat!.
*/
public class WaitFor5
{
    private static void Main()
    {
        int x = 0; //inicialitzo un variable amb un numero diferent a 5
        while (x != 5)// faig un bucle que hem digui que si no es 5 li demani un altre numero
        {
            Console.WriteLine("Escriume un 5");//printo per pantalla un text
            x = Convert.ToInt32(Console.ReadLine());//demano un numero
           
        }
        Console.WriteLine(x + " trobat!");//si el x es 5 surt del bucle y el printa per pantalla
    }
}