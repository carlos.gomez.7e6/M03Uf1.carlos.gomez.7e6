﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:L'usuari introdueix dos valors enters, el final i el salt

Escriu tots els numeros des de l'1 fins al final, amb una distància de salt
*/

public class CountWithJump
{
    private static void Main()
    {
        Console.WriteLine("Final");//printo per pantalla una ordre
        int x = Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo una ordre demanantli al usuari un valor
        Console.WriteLine("Salt");//printo per pantalla una ordre
        int y = Convert.ToInt32(Console.ReadLine());//declaro e inicialitzo una ordre demanantli al usuari un valor

        for (int s=1; s <= x; s += y)//faig un for declarant e incialitzant s comparantla amb x(el final) y sumantli y(els salts)
        {
            Console.Write(s + " ");//printo s
        }
    }
}
