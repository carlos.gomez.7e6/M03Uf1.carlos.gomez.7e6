﻿/*
* AUTHOR: Carlos Gomez
* DATE: 2022/10/10
* DESCRIPTION:Demanar a l'usuari un enter entre 1 i 5.

Si introdueix un número més gran o més petit, torna-li a demanar.

Mostra per pantalla: El número introduït: 3, (en cas de que l'usuari hagi introduït un 3) substituint el 3 pel número.
*/
public class NumberBetweenOneAndFive
{
    private static void Main()
    { 

        int x;//incialitzo un int y el declaro a 0
        do
        {
            Console.WriteLine("Donam un numero");//printo per pantalla
            x = Convert.ToInt32(Console.ReadLine());//demano una variable int al usuari
        }
        while (x > 5 || x < 1);//faig un bucle dient que x sigui mes gran 5 o mes petit que 1

        Console.WriteLine(x + " trobat");//si compleix amb la condicio anterior es el que numero ha siguit trobat    
         
    }
}
